require 'test_helper'

class TramosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tramo = tramos(:one)
  end

  test "should get index" do
    get tramos_url
    assert_response :success
  end

  test "should get new" do
    get new_tramo_url
    assert_response :success
  end

  test "should create tramo" do
    assert_difference('Tramo.count') do
      post tramos_url, params: { tramo: { desde: @tramo.desde, factor_cobro_id: @tramo.factor_cobro_id, hasta: @tramo.hasta, nombre: @tramo.nombre, tipo_tramo_id: @tramo.tipo_tramo_id } }
    end

    assert_redirected_to tramo_url(Tramo.last)
  end

  test "should show tramo" do
    get tramo_url(@tramo)
    assert_response :success
  end

  test "should get edit" do
    get edit_tramo_url(@tramo)
    assert_response :success
  end

  test "should update tramo" do
    patch tramo_url(@tramo), params: { tramo: { desde: @tramo.desde, factor_cobro_id: @tramo.factor_cobro_id, hasta: @tramo.hasta, nombre: @tramo.nombre, tipo_tramo_id: @tramo.tipo_tramo_id } }
    assert_redirected_to tramo_url(@tramo)
  end

  test "should destroy tramo" do
    assert_difference('Tramo.count', -1) do
      delete tramo_url(@tramo)
    end

    assert_redirected_to tramos_url
  end
end
