class Usuario < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable


  belongs_to :perfil
  has_one :empleado
	validates :email, uniqueness: {message: "Nombre de usuario ya se encuentra registrado"}
  has_and_belongs_to_many :sede
end
