require 'test_helper'

class SubempresaZonasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subempresa_zona = subempresa_zonas(:one)
  end

  test "should get index" do
    get subempresa_zonas_url
    assert_response :success
  end

  test "should get new" do
    get new_subempresa_zona_url
    assert_response :success
  end

  test "should create subempresa_zona" do
    assert_difference('SubempresaZona.count') do
      post subempresa_zonas_url, params: { subempresa_zona: { subempresa: @subempresa_zona.subempresa, zona_id: @subempresa_zona.zona_id } }
    end

    assert_redirected_to subempresa_zona_url(SubempresaZona.last)
  end

  test "should show subempresa_zona" do
    get subempresa_zona_url(@subempresa_zona)
    assert_response :success
  end

  test "should get edit" do
    get edit_subempresa_zona_url(@subempresa_zona)
    assert_response :success
  end

  test "should update subempresa_zona" do
    patch subempresa_zona_url(@subempresa_zona), params: { subempresa_zona: { subempresa: @subempresa_zona.subempresa, zona_id: @subempresa_zona.zona_id } }
    assert_redirected_to subempresa_zona_url(@subempresa_zona)
  end

  test "should destroy subempresa_zona" do
    assert_difference('SubempresaZona.count', -1) do
      delete subempresa_zona_url(@subempresa_zona)
    end

    assert_redirected_to subempresa_zonas_url
  end
end
