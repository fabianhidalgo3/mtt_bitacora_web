require 'test_helper'

class FactorComunasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @factor_comuna = factor_comunas(:one)
  end

  test "should get index" do
    get factor_comunas_url
    assert_response :success
  end

  test "should get new" do
    get new_factor_comuna_url
    assert_response :success
  end

  test "should create factor_comuna" do
    assert_difference('FactorComuna.count') do
      post factor_comunas_url, params: { factor_comuna: { comuna_id: @factor_comuna.comuna_id, factor_cobro_id: @factor_comuna.factor_cobro_id } }
    end

    assert_redirected_to factor_comuna_url(FactorComuna.last)
  end

  test "should show factor_comuna" do
    get factor_comuna_url(@factor_comuna)
    assert_response :success
  end

  test "should get edit" do
    get edit_factor_comuna_url(@factor_comuna)
    assert_response :success
  end

  test "should update factor_comuna" do
    patch factor_comuna_url(@factor_comuna), params: { factor_comuna: { comuna_id: @factor_comuna.comuna_id, factor_cobro_id: @factor_comuna.factor_cobro_id } }
    assert_redirected_to factor_comuna_url(@factor_comuna)
  end

  test "should destroy factor_comuna" do
    assert_difference('FactorComuna.count', -1) do
      delete factor_comuna_url(@factor_comuna)
    end

    assert_redirected_to factor_comunas_url
  end
end
