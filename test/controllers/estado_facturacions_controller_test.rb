require 'test_helper'

class EstadoFacturacionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @estado_facturacion = estado_facturacions(:one)
  end

  test "should get index" do
    get estado_facturacions_url
    assert_response :success
  end

  test "should get new" do
    get new_estado_facturacion_url
    assert_response :success
  end

  test "should create estado_facturacion" do
    assert_difference('EstadoFacturacion.count') do
      post estado_facturacions_url, params: { estado_facturacion: { nombre: @estado_facturacion.nombre } }
    end

    assert_redirected_to estado_facturacion_url(EstadoFacturacion.last)
  end

  test "should show estado_facturacion" do
    get estado_facturacion_url(@estado_facturacion)
    assert_response :success
  end

  test "should get edit" do
    get edit_estado_facturacion_url(@estado_facturacion)
    assert_response :success
  end

  test "should update estado_facturacion" do
    patch estado_facturacion_url(@estado_facturacion), params: { estado_facturacion: { nombre: @estado_facturacion.nombre } }
    assert_redirected_to estado_facturacion_url(@estado_facturacion)
  end

  test "should destroy estado_facturacion" do
    assert_difference('EstadoFacturacion.count', -1) do
      delete estado_facturacion_url(@estado_facturacion)
    end

    assert_redirected_to estado_facturacions_url
  end
end
