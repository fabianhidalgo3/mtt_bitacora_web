class CreatePuntoControls < ActiveRecord::Migration[5.0]
  def change
    create_table :punto_controls do |t|
      t.references :estado, foreign_key: true
      t.string :codigo
      t.string :nombre
      t.string :direccion
      t.string :calle
      t.string :numero
      t.string :interseccion
      t.references :comuna, foreign_key: true
      t.references :region, foreign_key: true
      t.decimal :latitud
      t.decimal :longitud
      t.datetime :fecha_modificacion

      t.timestamps
    end
  end
end
