require 'test_helper'

class ObservacionRepartosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @observacion_reparto = observacion_repartos(:one)
  end

  test "should get index" do
    get observacion_repartos_url
    assert_response :success
  end

  test "should get new" do
    get new_observacion_reparto_url
    assert_response :success
  end

  test "should create observacion_reparto" do
    assert_difference('ObservacionReparto.count') do
      post observacion_repartos_url, params: { observacion_reparto: { efectivo: @observacion_reparto.efectivo, observacion: @observacion_reparto.observacion } }
    end

    assert_redirected_to observacion_reparto_url(ObservacionReparto.last)
  end

  test "should show observacion_reparto" do
    get observacion_reparto_url(@observacion_reparto)
    assert_response :success
  end

  test "should get edit" do
    get edit_observacion_reparto_url(@observacion_reparto)
    assert_response :success
  end

  test "should update observacion_reparto" do
    patch observacion_reparto_url(@observacion_reparto), params: { observacion_reparto: { efectivo: @observacion_reparto.efectivo, observacion: @observacion_reparto.observacion } }
    assert_redirected_to observacion_reparto_url(@observacion_reparto)
  end

  test "should destroy observacion_reparto" do
    assert_difference('ObservacionReparto.count', -1) do
      delete observacion_reparto_url(@observacion_reparto)
    end

    assert_redirected_to observacion_repartos_url
  end
end
