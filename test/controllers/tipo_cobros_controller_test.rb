require 'test_helper'

class TipoCobrosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_cobro = tipo_cobros(:one)
  end

  test "should get index" do
    get tipo_cobros_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_cobro_url
    assert_response :success
  end

  test "should create tipo_cobro" do
    assert_difference('TipoCobro.count') do
      post tipo_cobros_url, params: { tipo_cobro: { descripcion: @tipo_cobro.descripcion } }
    end

    assert_redirected_to tipo_cobro_url(TipoCobro.last)
  end

  test "should show tipo_cobro" do
    get tipo_cobro_url(@tipo_cobro)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_cobro_url(@tipo_cobro)
    assert_response :success
  end

  test "should update tipo_cobro" do
    patch tipo_cobro_url(@tipo_cobro), params: { tipo_cobro: { descripcion: @tipo_cobro.descripcion } }
    assert_redirected_to tipo_cobro_url(@tipo_cobro)
  end

  test "should destroy tipo_cobro" do
    assert_difference('TipoCobro.count', -1) do
      delete tipo_cobro_url(@tipo_cobro)
    end

    assert_redirected_to tipo_cobros_url
  end
end
