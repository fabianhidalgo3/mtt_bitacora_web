class CreateOrdenTrabajos < ActiveRecord::Migration[5.0]
  def change
    create_table :orden_trabajos do |t|
      t.datetime :fecha_inicio
      t.datetime :fechatermino
      t.references :estado, foreign_key: true
      t.references :tarea, foreign_key: true
      t.references :tipo_servicio, foreign_key: true
      t.references :punto_control, foreign_key: true
      t.references :comuna, foreign_key: true
      t.references :region, foreign_key: true
      t.references :turno, foreign_key: true
      t.references :sentido, foreign_key: true
      #t.references :direccion, foreign_key: true
      t.integer :direccion_id
      t.references :sede, foreign_key: true
      t.integer :correlativo_solicitud
      t.datetime :fecha_modificacion

      t.timestamps
    end
  end
end
