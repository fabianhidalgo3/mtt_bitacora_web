class CreateEstadisticas < ActiveRecord::Migration[5.0]
  def change
    create_table :estadisticas do |t|
      t.integer :sistema_id
      t.integer :tarea_id
      t.date :fecha
      t.time :hora
      t.integer :controles
      t.integer :partes
      t.decimal :latitud
      t.decimal :longitud
      t.integer :evento_id
      t.string :observacion
      t.integer :inspector_id
      t.integer :imei

      t.timestamps
    end
  end
end
