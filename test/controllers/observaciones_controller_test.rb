require 'test_helper'

class ObservacionesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @observacione = observaciones(:one)
  end

  test "should get index" do
    get observaciones_url
    assert_response :success
  end

  test "should get new" do
    get new_observacione_url
    assert_response :success
  end

  test "should create observacione" do
    assert_difference('Observacione.count') do
      post observaciones_url, params: { observacione: { clave_lectura_id: @observacione.clave_lectura_id, descripcion_id: @observacione.descripcion_id } }
    end

    assert_redirected_to observacione_url(Observacione.last)
  end

  test "should show observacione" do
    get observacione_url(@observacione)
    assert_response :success
  end

  test "should get edit" do
    get edit_observacione_url(@observacione)
    assert_response :success
  end

  test "should update observacione" do
    patch observacione_url(@observacione), params: { observacione: { clave_lectura_id: @observacione.clave_lectura_id, descripcion_id: @observacione.descripcion_id } }
    assert_redirected_to observacione_url(@observacione)
  end

  test "should destroy observacione" do
    assert_difference('Observacione.count', -1) do
      delete observacione_url(@observacione)
    end

    assert_redirected_to observaciones_url
  end
end
