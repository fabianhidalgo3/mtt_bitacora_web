class CreateSentidos < ActiveRecord::Migration[5.0]
  def change
    create_table :sentidos do |t|
      t.string :descripcion

      t.timestamps
    end
  end
end
