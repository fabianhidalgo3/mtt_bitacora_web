require 'test_helper'

class ContratistaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contratistum = contratista(:one)
  end

  test "should get index" do
    get contratista_url
    assert_response :success
  end

  test "should get new" do
    get new_contratistum_url
    assert_response :success
  end

  test "should create contratistum" do
    assert_difference('Contratistum.count') do
      post contratista_url, params: { contratistum: { nombre: @contratistum.nombre } }
    end

    assert_redirected_to contratistum_url(Contratistum.last)
  end

  test "should show contratistum" do
    get contratistum_url(@contratistum)
    assert_response :success
  end

  test "should get edit" do
    get edit_contratistum_url(@contratistum)
    assert_response :success
  end

  test "should update contratistum" do
    patch contratistum_url(@contratistum), params: { contratistum: { nombre: @contratistum.nombre } }
    assert_redirected_to contratistum_url(@contratistum)
  end

  test "should destroy contratistum" do
    assert_difference('Contratistum.count', -1) do
      delete contratistum_url(@contratistum)
    end

    assert_redirected_to contratista_url
  end
end
