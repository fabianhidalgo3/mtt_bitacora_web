require 'test_helper'

class EstadoRepartosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @estado_reparto = estado_repartos(:one)
  end

  test "should get index" do
    get estado_repartos_url
    assert_response :success
  end

  test "should get new" do
    get new_estado_reparto_url
    assert_response :success
  end

  test "should create estado_reparto" do
    assert_difference('EstadoReparto.count') do
      post estado_repartos_url, params: { estado_reparto: { estado: @estado_reparto.estado } }
    end

    assert_redirected_to estado_reparto_url(EstadoReparto.last)
  end

  test "should show estado_reparto" do
    get estado_reparto_url(@estado_reparto)
    assert_response :success
  end

  test "should get edit" do
    get edit_estado_reparto_url(@estado_reparto)
    assert_response :success
  end

  test "should update estado_reparto" do
    patch estado_reparto_url(@estado_reparto), params: { estado_reparto: { estado: @estado_reparto.estado } }
    assert_redirected_to estado_reparto_url(@estado_reparto)
  end

  test "should destroy estado_reparto" do
    assert_difference('EstadoReparto.count', -1) do
      delete estado_reparto_url(@estado_reparto)
    end

    assert_redirected_to estado_repartos_url
  end
end
