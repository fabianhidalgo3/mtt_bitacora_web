require 'test_helper'

class OrdenRepartosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @orden_reparto = orden_repartos(:one)
  end

  test "should get index" do
    get orden_repartos_url
    assert_response :success
  end

  test "should get new" do
    get new_orden_reparto_url
    assert_response :success
  end

  test "should create orden_reparto" do
    assert_difference('OrdenReparto.count') do
      post orden_repartos_url, params: { orden_reparto: { cliente_id: @orden_reparto.cliente_id, comuna_id: @orden_reparto.comuna_id, correlativo_impresion: @orden_reparto.correlativo_impresion, direccion_entrega: @orden_reparto.direccion_entrega, estado_reparto_id: @orden_reparto.estado_reparto_id, fecha_emision: @orden_reparto.fecha_emision, fecha_entrega: @orden_reparto.fecha_entrega, fecha_vencimiento: @orden_reparto.fecha_vencimiento, numero_boleta: @orden_reparto.numero_boleta, numero_interno: @orden_reparto.numero_interno, observacion_reparto_id: @orden_reparto.observacion_reparto_id, orden_ruta: @orden_reparto.orden_ruta, ruta_reparto_id: @orden_reparto.ruta_reparto_id, tipo_documento_id: @orden_reparto.tipo_documento_id, tipo_entrega_id: @orden_reparto.tipo_entrega_id, tipo_reparto_id: @orden_reparto.tipo_reparto_id, total_pago: @orden_reparto.total_pago } }
    end

    assert_redirected_to orden_reparto_url(OrdenReparto.last)
  end

  test "should show orden_reparto" do
    get orden_reparto_url(@orden_reparto)
    assert_response :success
  end

  test "should get edit" do
    get edit_orden_reparto_url(@orden_reparto)
    assert_response :success
  end

  test "should update orden_reparto" do
    patch orden_reparto_url(@orden_reparto), params: { orden_reparto: { cliente_id: @orden_reparto.cliente_id, comuna_id: @orden_reparto.comuna_id, correlativo_impresion: @orden_reparto.correlativo_impresion, direccion_entrega: @orden_reparto.direccion_entrega, estado_reparto_id: @orden_reparto.estado_reparto_id, fecha_emision: @orden_reparto.fecha_emision, fecha_entrega: @orden_reparto.fecha_entrega, fecha_vencimiento: @orden_reparto.fecha_vencimiento, numero_boleta: @orden_reparto.numero_boleta, numero_interno: @orden_reparto.numero_interno, observacion_reparto_id: @orden_reparto.observacion_reparto_id, orden_ruta: @orden_reparto.orden_ruta, ruta_reparto_id: @orden_reparto.ruta_reparto_id, tipo_documento_id: @orden_reparto.tipo_documento_id, tipo_entrega_id: @orden_reparto.tipo_entrega_id, tipo_reparto_id: @orden_reparto.tipo_reparto_id, total_pago: @orden_reparto.total_pago } }
    assert_redirected_to orden_reparto_url(@orden_reparto)
  end

  test "should destroy orden_reparto" do
    assert_difference('OrdenReparto.count', -1) do
      delete orden_reparto_url(@orden_reparto)
    end

    assert_redirected_to orden_repartos_url
  end
end
