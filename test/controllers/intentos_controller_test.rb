require 'test_helper'

class IntentosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @intento = intentos(:one)
  end

  test "should get index" do
    get intentos_url
    assert_response :success
  end

  test "should get new" do
    get new_intento_url
    assert_response :success
  end

  test "should create intento" do
    assert_difference('Intento.count') do
      post intentos_url, params: { intento: { detalle_orden_lectura_id: @intento.detalle_orden_lectura_id, lectura: @intento.lectura } }
    end

    assert_redirected_to intento_url(Intento.last)
  end

  test "should show intento" do
    get intento_url(@intento)
    assert_response :success
  end

  test "should get edit" do
    get edit_intento_url(@intento)
    assert_response :success
  end

  test "should update intento" do
    patch intento_url(@intento), params: { intento: { detalle_orden_lectura_id: @intento.detalle_orden_lectura_id, lectura: @intento.lectura } }
    assert_redirected_to intento_url(@intento)
  end

  test "should destroy intento" do
    assert_difference('Intento.count', -1) do
      delete intento_url(@intento)
    end

    assert_redirected_to intentos_url
  end
end
