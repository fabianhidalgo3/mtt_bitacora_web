class CreateUsuariosTareas < ActiveRecord::Migration[5.0]
  def change
    create_table :usuarios_tareas do |t|
      t.references :usuario, foreign_key: true
      t.references :sede, foreign_key: true
      t.references :region, foreign_key: true
      t.integer :numero_ot

      t.timestamps
    end
  end
end
