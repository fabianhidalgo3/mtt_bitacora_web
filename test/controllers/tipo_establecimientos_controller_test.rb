require 'test_helper'

class TipoEstablecimientosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_establecimiento = tipo_establecimientos(:one)
  end

  test "should get index" do
    get tipo_establecimientos_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_establecimiento_url
    assert_response :success
  end

  test "should create tipo_establecimiento" do
    assert_difference('TipoEstablecimiento.count') do
      post tipo_establecimientos_url, params: { tipo_establecimiento: { codigo: @tipo_establecimiento.codigo, excento: @tipo_establecimiento.excento, facturacion_en_terreno: @tipo_establecimiento.facturacion_en_terreno, nombre: @tipo_establecimiento.nombre } }
    end

    assert_redirected_to tipo_establecimiento_url(TipoEstablecimiento.last)
  end

  test "should show tipo_establecimiento" do
    get tipo_establecimiento_url(@tipo_establecimiento)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_establecimiento_url(@tipo_establecimiento)
    assert_response :success
  end

  test "should update tipo_establecimiento" do
    patch tipo_establecimiento_url(@tipo_establecimiento), params: { tipo_establecimiento: { codigo: @tipo_establecimiento.codigo, excento: @tipo_establecimiento.excento, facturacion_en_terreno: @tipo_establecimiento.facturacion_en_terreno, nombre: @tipo_establecimiento.nombre } }
    assert_redirected_to tipo_establecimiento_url(@tipo_establecimiento)
  end

  test "should destroy tipo_establecimiento" do
    assert_difference('TipoEstablecimiento.count', -1) do
      delete tipo_establecimiento_url(@tipo_establecimiento)
    end

    assert_redirected_to tipo_establecimientos_url
  end
end
