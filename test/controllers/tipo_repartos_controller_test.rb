require 'test_helper'

class TipoRepartosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_reparto = tipo_repartos(:one)
  end

  test "should get index" do
    get tipo_repartos_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_reparto_url
    assert_response :success
  end

  test "should create tipo_reparto" do
    assert_difference('TipoReparto.count') do
      post tipo_repartos_url, params: { tipo_reparto: { descripcion: @tipo_reparto.descripcion } }
    end

    assert_redirected_to tipo_reparto_url(TipoReparto.last)
  end

  test "should show tipo_reparto" do
    get tipo_reparto_url(@tipo_reparto)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_reparto_url(@tipo_reparto)
    assert_response :success
  end

  test "should update tipo_reparto" do
    patch tipo_reparto_url(@tipo_reparto), params: { tipo_reparto: { descripcion: @tipo_reparto.descripcion } }
    assert_redirected_to tipo_reparto_url(@tipo_reparto)
  end

  test "should destroy tipo_reparto" do
    assert_difference('TipoReparto.count', -1) do
      delete tipo_reparto_url(@tipo_reparto)
    end

    assert_redirected_to tipo_repartos_url
  end
end
