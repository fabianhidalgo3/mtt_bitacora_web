require 'test_helper'

class EmpleadoZonasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @empleado_zona = empleado_zonas(:one)
  end

  test "should get index" do
    get empleado_zonas_url
    assert_response :success
  end

  test "should get new" do
    get new_empleado_zona_url
    assert_response :success
  end

  test "should create empleado_zona" do
    assert_difference('EmpleadoZona.count') do
      post empleado_zonas_url, params: { empleado_zona: { empleado_id: @empleado_zona.empleado_id, zona_id: @empleado_zona.zona_id } }
    end

    assert_redirected_to empleado_zona_url(EmpleadoZona.last)
  end

  test "should show empleado_zona" do
    get empleado_zona_url(@empleado_zona)
    assert_response :success
  end

  test "should get edit" do
    get edit_empleado_zona_url(@empleado_zona)
    assert_response :success
  end

  test "should update empleado_zona" do
    patch empleado_zona_url(@empleado_zona), params: { empleado_zona: { empleado_id: @empleado_zona.empleado_id, zona_id: @empleado_zona.zona_id } }
    assert_redirected_to empleado_zona_url(@empleado_zona)
  end

  test "should destroy empleado_zona" do
    assert_difference('EmpleadoZona.count', -1) do
      delete empleado_zona_url(@empleado_zona)
    end

    assert_redirected_to empleado_zonas_url
  end
end
