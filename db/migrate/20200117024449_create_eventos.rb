class CreateEventos < ActiveRecord::Migration[5.0]
  def change
    create_table :eventos do |t|
      t.string :nombre
      t.integer :color

      t.timestamps
    end
  end
end
