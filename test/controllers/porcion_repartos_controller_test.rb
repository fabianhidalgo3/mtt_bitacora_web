require 'test_helper'

class PorcionRepartosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @porcion_reparto = porcion_repartos(:one)
  end

  test "should get index" do
    get porcion_repartos_url
    assert_response :success
  end

  test "should get new" do
    get new_porcion_reparto_url
    assert_response :success
  end

  test "should create porcion_reparto" do
    assert_difference('PorcionReparto.count') do
      post porcion_repartos_url, params: { porcion_reparto: { ano: @porcion_reparto.ano, codigo: @porcion_reparto.codigo, mes: @porcion_reparto.mes, nombre: @porcion_reparto.nombre, subempresa_id: @porcion_reparto.subempresa_id, zona_id: @porcion_reparto.zona_id } }
    end

    assert_redirected_to porcion_reparto_url(PorcionReparto.last)
  end

  test "should show porcion_reparto" do
    get porcion_reparto_url(@porcion_reparto)
    assert_response :success
  end

  test "should get edit" do
    get edit_porcion_reparto_url(@porcion_reparto)
    assert_response :success
  end

  test "should update porcion_reparto" do
    patch porcion_reparto_url(@porcion_reparto), params: { porcion_reparto: { ano: @porcion_reparto.ano, codigo: @porcion_reparto.codigo, mes: @porcion_reparto.mes, nombre: @porcion_reparto.nombre, subempresa_id: @porcion_reparto.subempresa_id, zona_id: @porcion_reparto.zona_id } }
    assert_redirected_to porcion_reparto_url(@porcion_reparto)
  end

  test "should destroy porcion_reparto" do
    assert_difference('PorcionReparto.count', -1) do
      delete porcion_reparto_url(@porcion_reparto)
    end

    assert_redirected_to porcion_repartos_url
  end
end
