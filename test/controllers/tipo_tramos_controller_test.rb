require 'test_helper'

class TipoTramosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_tramo = tipo_tramos(:one)
  end

  test "should get index" do
    get tipo_tramos_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_tramo_url
    assert_response :success
  end

  test "should create tipo_tramo" do
    assert_difference('TipoTramo.count') do
      post tipo_tramos_url, params: { tipo_tramo: { nombre: @tipo_tramo.nombre } }
    end

    assert_redirected_to tipo_tramo_url(TipoTramo.last)
  end

  test "should show tipo_tramo" do
    get tipo_tramo_url(@tipo_tramo)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_tramo_url(@tipo_tramo)
    assert_response :success
  end

  test "should update tipo_tramo" do
    patch tipo_tramo_url(@tipo_tramo), params: { tipo_tramo: { nombre: @tipo_tramo.nombre } }
    assert_redirected_to tipo_tramo_url(@tipo_tramo)
  end

  test "should destroy tipo_tramo" do
    assert_difference('TipoTramo.count', -1) do
      delete tipo_tramo_url(@tipo_tramo)
    end

    assert_redirected_to tipo_tramos_url
  end
end
