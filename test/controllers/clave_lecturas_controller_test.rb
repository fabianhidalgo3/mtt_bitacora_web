require 'test_helper'

class ClaveLecturasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @clave_lectura = clave_lecturas(:one)
  end

  test "should get index" do
    get clave_lecturas_url
    assert_response :success
  end

  test "should get new" do
    get new_clave_lectura_url
    assert_response :success
  end

  test "should create clave_lectura" do
    assert_difference('ClaveLectura.count') do
      post clave_lecturas_url, params: { clave_lectura: { nombre: @clave_lectura.nombre, tipo_cobro_id: @clave_lectura.tipo_cobro_id } }
    end

    assert_redirected_to clave_lectura_url(ClaveLectura.last)
  end

  test "should show clave_lectura" do
    get clave_lectura_url(@clave_lectura)
    assert_response :success
  end

  test "should get edit" do
    get edit_clave_lectura_url(@clave_lectura)
    assert_response :success
  end

  test "should update clave_lectura" do
    patch clave_lectura_url(@clave_lectura), params: { clave_lectura: { nombre: @clave_lectura.nombre, tipo_cobro_id: @clave_lectura.tipo_cobro_id } }
    assert_redirected_to clave_lectura_url(@clave_lectura)
  end

  test "should destroy clave_lectura" do
    assert_difference('ClaveLectura.count', -1) do
      delete clave_lectura_url(@clave_lectura)
    end

    assert_redirected_to clave_lecturas_url
  end
end
