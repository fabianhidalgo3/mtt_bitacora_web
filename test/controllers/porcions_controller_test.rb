require 'test_helper'

class PorcionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @porcion = porcions(:one)
  end

  test "should get index" do
    get porcions_url
    assert_response :success
  end

  test "should get new" do
    get new_porcion_url
    assert_response :success
  end

  test "should create porcion" do
    assert_difference('Porcion.count') do
      post porcions_url, params: { porcion: { ano: @porcion.ano, codigo: @porcion.codigo, mes: @porcion.mes, nombre: @porcion.nombre, subempresa_id: @porcion.subempresa_id, zona_id: @porcion.zona_id } }
    end

    assert_redirected_to porcion_url(Porcion.last)
  end

  test "should show porcion" do
    get porcion_url(@porcion)
    assert_response :success
  end

  test "should get edit" do
    get edit_porcion_url(@porcion)
    assert_response :success
  end

  test "should update porcion" do
    patch porcion_url(@porcion), params: { porcion: { ano: @porcion.ano, codigo: @porcion.codigo, mes: @porcion.mes, nombre: @porcion.nombre, subempresa_id: @porcion.subempresa_id, zona_id: @porcion.zona_id } }
    assert_redirected_to porcion_url(@porcion)
  end

  test "should destroy porcion" do
    assert_difference('Porcion.count', -1) do
      delete porcion_url(@porcion)
    end

    assert_redirected_to porcions_url
  end
end
