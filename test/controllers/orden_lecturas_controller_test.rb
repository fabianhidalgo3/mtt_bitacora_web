require 'test_helper'

class OrdenLecturasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @orden_lectura = orden_lecturas(:one)
  end

  test "should get index" do
    get orden_lecturas_url
    assert_response :success
  end

  test "should get new" do
    get new_orden_lectura_url
    assert_response :success
  end

  test "should create orden_lectura" do
    assert_difference('OrdenLectura.count') do
      post orden_lecturas_url, params: { orden_lectura: { ajuste_sencillo_actual: @orden_lectura.ajuste_sencillo_actual, ajuste_sencillo_anterior: @orden_lectura.ajuste_sencillo_anterior, cliente_id: @orden_lectura.cliente_id, codigo: @orden_lectura.codigo, direccion: @orden_lectura.direccion, direccion_entrega: @orden_lectura.direccion_entrega, estado_lectura_id: @orden_lectura.estado_lectura_id, fecha_asignacion: @orden_lectura.fecha_asignacion, fecha_carga: @orden_lectura.fecha_carga, fecha_propuesta: @orden_lectura.fecha_propuesta, gps_latitud: @orden_lectura.gps_latitud, gps_longitud: @orden_lectura.gps_longitud, instalacion_id: @orden_lectura.instalacion_id, localidad_id: @orden_lectura.localidad_id, medidor_id: @orden_lectura.medidor_id, numero_poste: @orden_lectura.numero_poste, posicion: @orden_lectura.posicion, rutum_id: @orden_lectura.rutum_id, sap_id: @orden_lectura.sap_id, tipo_entrega_id: @orden_lectura.tipo_entrega_id, tipo_establecimiento_id: @orden_lectura.tipo_establecimiento_id, tipo_lectura_id: @orden_lectura.tipo_lectura_id, tipo_tarifa_id: @orden_lectura.tipo_tarifa_id } }
    end

    assert_redirected_to orden_lectura_url(OrdenLectura.last)
  end

  test "should show orden_lectura" do
    get orden_lectura_url(@orden_lectura)
    assert_response :success
  end

  test "should get edit" do
    get edit_orden_lectura_url(@orden_lectura)
    assert_response :success
  end

  test "should update orden_lectura" do
    patch orden_lectura_url(@orden_lectura), params: { orden_lectura: { ajuste_sencillo_actual: @orden_lectura.ajuste_sencillo_actual, ajuste_sencillo_anterior: @orden_lectura.ajuste_sencillo_anterior, cliente_id: @orden_lectura.cliente_id, codigo: @orden_lectura.codigo, direccion: @orden_lectura.direccion, direccion_entrega: @orden_lectura.direccion_entrega, estado_lectura_id: @orden_lectura.estado_lectura_id, fecha_asignacion: @orden_lectura.fecha_asignacion, fecha_carga: @orden_lectura.fecha_carga, fecha_propuesta: @orden_lectura.fecha_propuesta, gps_latitud: @orden_lectura.gps_latitud, gps_longitud: @orden_lectura.gps_longitud, instalacion_id: @orden_lectura.instalacion_id, localidad_id: @orden_lectura.localidad_id, medidor_id: @orden_lectura.medidor_id, numero_poste: @orden_lectura.numero_poste, posicion: @orden_lectura.posicion, rutum_id: @orden_lectura.rutum_id, sap_id: @orden_lectura.sap_id, tipo_entrega_id: @orden_lectura.tipo_entrega_id, tipo_establecimiento_id: @orden_lectura.tipo_establecimiento_id, tipo_lectura_id: @orden_lectura.tipo_lectura_id, tipo_tarifa_id: @orden_lectura.tipo_tarifa_id } }
    assert_redirected_to orden_lectura_url(@orden_lectura)
  end

  test "should destroy orden_lectura" do
    assert_difference('OrdenLectura.count', -1) do
      delete orden_lectura_url(@orden_lectura)
    end

    assert_redirected_to orden_lecturas_url
  end
end
