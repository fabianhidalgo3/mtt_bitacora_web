Rails.application.routes.draw do


  devise_for :usuarios
  root 'monitor_inspectores#index'
  resources :monitor_inspectores do 
    collection do 
      get 'carga_comunas'
      get 'carga_usuarios'
      get 'carga_sedes'
    end
  end

  # > Rutas Catastros
  # > Perfiles de Usuarios
  resources :perfiles do
    collection do
      get 'exportar_csv'
      get 'exportar_pdf'
    end
  end
  match 'catastro/perfiles' => 'perfils#index', via: [:get, :post]

  # > Usuarios Administrativos
  resources :usuarios_administrativos do
    collection do
    	post 'cambiar'
      get 'modificar_contrasena'
      get 'exportar_pdf'
      get 'exportar_csv'
    end
  end
  match 'catastro/empleados/administrativos' => 'usuarios_administrativos#index', via: [:post, :get]

  # > Usuarios Agente de Terreno
  resources :usuarios_agentes_terreno do
    collection do
    	post 'cambiar'
      get 'modificar_contrasena'
      get 'exportar_pdf'
      get 'exportar_csv'
    end
  end
  match 'catastro/empleados/agentes_terreno' => 'usuarios_agentes_terreno#index', via: [:post, :get]

end
