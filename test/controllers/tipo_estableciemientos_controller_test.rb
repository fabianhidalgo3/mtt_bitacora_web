require 'test_helper'

class TipoEstableciemientosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_estableciemiento = tipo_estableciemientos(:one)
  end

  test "should get index" do
    get tipo_estableciemientos_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_estableciemiento_url
    assert_response :success
  end

  test "should create tipo_estableciemiento" do
    assert_difference('TipoEstableciemiento.count') do
      post tipo_estableciemientos_url, params: { tipo_estableciemiento: { codigo: @tipo_estableciemiento.codigo, excento: @tipo_estableciemiento.excento, facturacion_en_terreno: @tipo_estableciemiento.facturacion_en_terreno, nombre: @tipo_estableciemiento.nombre } }
    end

    assert_redirected_to tipo_estableciemiento_url(TipoEstableciemiento.last)
  end

  test "should show tipo_estableciemiento" do
    get tipo_estableciemiento_url(@tipo_estableciemiento)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_estableciemiento_url(@tipo_estableciemiento)
    assert_response :success
  end

  test "should update tipo_estableciemiento" do
    patch tipo_estableciemiento_url(@tipo_estableciemiento), params: { tipo_estableciemiento: { codigo: @tipo_estableciemiento.codigo, excento: @tipo_estableciemiento.excento, facturacion_en_terreno: @tipo_estableciemiento.facturacion_en_terreno, nombre: @tipo_estableciemiento.nombre } }
    assert_redirected_to tipo_estableciemiento_url(@tipo_estableciemiento)
  end

  test "should destroy tipo_estableciemiento" do
    assert_difference('TipoEstableciemiento.count', -1) do
      delete tipo_estableciemiento_url(@tipo_estableciemiento)
    end

    assert_redirected_to tipo_estableciemientos_url
  end
end
