# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019120314382611) do

  create_table "asignacion_ordens", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "orden_trabajo_id"
    t.integer  "usuario_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["orden_trabajo_id"], name: "index_asignacion_ordens_on_orden_trabajo_id", using: :btree
    t.index ["usuario_id"], name: "index_asignacion_ordens_on_usuario_id", using: :btree
  end

  create_table "comunas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.integer  "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_comunas_on_region_id", using: :btree
  end

  create_table "direccions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "direccion"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estadisticas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "sistema_id"
    t.integer  "tarea_id"
    t.date     "fecha"
    t.time     "hora"
    t.integer  "controles"
    t.integer  "partes"
    t.decimal  "latitud",      precision: 10
    t.decimal  "longitud",     precision: 10
    t.integer  "evento_id"
    t.string   "observacion"
    t.integer  "inspector_id"
    t.integer  "imei"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "estado_punto_controls", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "estados", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "eventos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.integer  "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orden_trabajos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "fecha_inicio"
    t.datetime "fechatermino"
    t.integer  "estado_id"
    t.integer  "tarea_id"
    t.integer  "tipo_servicio_id"
    t.integer  "punto_control_id"
    t.integer  "comuna_id"
    t.integer  "region_id"
    t.integer  "turno_id"
    t.integer  "sentido_id"
    t.integer  "direccion_id"
    t.integer  "sede_id"
    t.integer  "correlativo_solicitud"
    t.datetime "fecha_modificacion"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["comuna_id"], name: "index_orden_trabajos_on_comuna_id", using: :btree
    t.index ["estado_id"], name: "index_orden_trabajos_on_estado_id", using: :btree
    t.index ["punto_control_id"], name: "index_orden_trabajos_on_punto_control_id", using: :btree
    t.index ["region_id"], name: "index_orden_trabajos_on_region_id", using: :btree
    t.index ["sede_id"], name: "index_orden_trabajos_on_sede_id", using: :btree
    t.index ["sentido_id"], name: "index_orden_trabajos_on_sentido_id", using: :btree
    t.index ["tarea_id"], name: "index_orden_trabajos_on_tarea_id", using: :btree
    t.index ["tipo_servicio_id"], name: "index_orden_trabajos_on_tipo_servicio_id", using: :btree
    t.index ["turno_id"], name: "index_orden_trabajos_on_turno_id", using: :btree
  end

  create_table "perfils", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "punto_controls", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "estado_id"
    t.string   "codigo"
    t.string   "nombre"
    t.string   "direccion"
    t.string   "calle"
    t.string   "numero"
    t.string   "interseccion"
    t.integer  "comuna_id"
    t.integer  "region_id"
    t.decimal  "latitud",            precision: 10
    t.decimal  "longitud",           precision: 10
    t.datetime "fecha_modificacion"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["comuna_id"], name: "index_punto_controls_on_comuna_id", using: :btree
    t.index ["estado_id"], name: "index_punto_controls_on_estado_id", using: :btree
    t.index ["region_id"], name: "index_punto_controls_on_region_id", using: :btree
  end

  create_table "regions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sedes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.integer  "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_sedes_on_region_id", using: :btree
  end

  create_table "sedes_usuarios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "sede_id"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sede_id"], name: "index_sedes_usuarios_on_sede_id", using: :btree
    t.index ["usuario_id"], name: "index_sedes_usuarios_on_usuario_id", using: :btree
  end

  create_table "sentidos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "tareas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_servicios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "turnos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "usuarios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "codigo"
    t.integer  "sede_id"
    t.integer  "region_id"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "perfil_id"
    t.index ["email"], name: "index_usuarios_on_email", unique: true, using: :btree
    t.index ["perfil_id"], name: "index_usuarios_on_perfil_id", using: :btree
    t.index ["region_id"], name: "index_usuarios_on_region_id", using: :btree
    t.index ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true, using: :btree
    t.index ["sede_id"], name: "index_usuarios_on_sede_id", using: :btree
  end

  create_table "usuarios_tareas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "usuario_id"
    t.integer  "sede_id"
    t.integer  "region_id"
    t.integer  "numero_ot"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_usuarios_tareas_on_region_id", using: :btree
    t.index ["sede_id"], name: "index_usuarios_tareas_on_sede_id", using: :btree
    t.index ["usuario_id"], name: "index_usuarios_tareas_on_usuario_id", using: :btree
  end

  add_foreign_key "asignacion_ordens", "usuarios"
  add_foreign_key "comunas", "regions"
  add_foreign_key "orden_trabajos", "comunas"
  add_foreign_key "orden_trabajos", "estados"
  add_foreign_key "orden_trabajos", "punto_controls"
  add_foreign_key "orden_trabajos", "regions"
  add_foreign_key "orden_trabajos", "sedes"
  add_foreign_key "orden_trabajos", "sentidos"
  add_foreign_key "orden_trabajos", "tareas"
  add_foreign_key "orden_trabajos", "tipo_servicios"
  add_foreign_key "orden_trabajos", "turnos"
  add_foreign_key "punto_controls", "comunas"
  add_foreign_key "punto_controls", "estados"
  add_foreign_key "punto_controls", "regions"
  add_foreign_key "sedes", "regions"
  add_foreign_key "sedes_usuarios", "sedes"
  add_foreign_key "sedes_usuarios", "usuarios"
  add_foreign_key "usuarios", "perfils"
  add_foreign_key "usuarios", "regions"
  add_foreign_key "usuarios", "sedes"
  add_foreign_key "usuarios_tareas", "regions"
  add_foreign_key "usuarios_tareas", "sedes"
  add_foreign_key "usuarios_tareas", "usuarios"
end
