class CicloLectorPdf < Prawn::Document
  include Prawn::View
  require 'prawn-styled-text'

  def initialize(lista, totales)
    super :page_size => "A4", :page_layout => :landscape
    @lista = lista
    @totales = totales
    fuentes
    header
    body
    footer 
  end

  def header    
    image "#{Rails.root}/public/images/logo_chilquinta_gris.jpg",:position => :right,  :width => 90, :heigth => 30
    styled_text '<h4 style="text-align: left;"><b>REPORTE:</b> EFECTIVIDAD POR LECTOR  </h4> <hr>'
  end
  

  def body
    move_down 20
    cant_lista = lista.count
    table lista do
      row(0).size = 11
      row(0).background_color = '555658'
      row(0).text_color = 'ffffff'
      row(0).font_style = :bold
      columns(0..10).align = :center
      columns(0..3).font_style = :bold
      row(1..cant_lista).size = 8
      row(cant_lista -1).background_color= '555658'
      row(cant_lista -1).text_color = 'ffffff'
      row(cant_lista -1).font_style = :bold
      row(cant_lista -1).size = 11
      self.header = true
      self.cells.borders = [:top, :bottom, :left, :right]
      self.column_widths = [60,60,70,60,80,80,70,90,70,40,70]
    end
  end
  

  def footer
    move_down 40
    styled_text '<p style="text-align:right">' + "Fecha descarga: " + Time.now.strftime("%d/%m/%Y %H:%M:%S").to_s + "</p>" 
  end

  def lista
    [['Ciclo', 'Comuna', 'Lector', 'Ruta', 'Medidores asignados', 'Lecturas asignadas','Claves efectivas','Claves improcedentes','Problemas técnicos','Total','Efectividad']] +
    @lista.map do |l|
      [l[0], l[1],l[2], l[3], l[4],l[5],l[6],l[7],l[8],l[9],l[10].to_s+ " %"]
    end
  end

  def fuentes
    font_families.update("Roboto" => {
      :normal => "public/assets/fonts/Roboto-Regular.ttf",
      :italic => "public/assets/fonts/Roboto-Italic.ttf",
      :bold => "public/assets/fonts/Roboto-Bold.ttf",
    })
    font "Roboto"
  end
  
end  