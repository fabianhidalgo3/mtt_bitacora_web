class CreateEstadoPuntoControls < ActiveRecord::Migration[5.0]
  def change
    create_table :estado_punto_controls do |t|
      t.string :descripcion

      t.timestamps
    end
  end
end
