class OrdenTrabajo < ApplicationRecord
  belongs_to :estado_punto_control
  belongs_to :tarea
  belongs_to :tipo_servivio
  belongs_to :punto_control
  belongs_to :comuna
  belongs_to :region
  belongs_to :turno
  belongs_to :sentido
  belongs_to :sede
  belongs_to :fecha_modificacion
end
