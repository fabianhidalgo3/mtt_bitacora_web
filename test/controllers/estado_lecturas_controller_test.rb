require 'test_helper'

class EstadoLecturasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @estado_lectura = estado_lecturas(:one)
  end

  test "should get index" do
    get estado_lecturas_url
    assert_response :success
  end

  test "should get new" do
    get new_estado_lectura_url
    assert_response :success
  end

  test "should create estado_lectura" do
    assert_difference('EstadoLectura.count') do
      post estado_lecturas_url, params: { estado_lectura: { nombre: @estado_lectura.nombre } }
    end

    assert_redirected_to estado_lectura_url(EstadoLectura.last)
  end

  test "should show estado_lectura" do
    get estado_lectura_url(@estado_lectura)
    assert_response :success
  end

  test "should get edit" do
    get edit_estado_lectura_url(@estado_lectura)
    assert_response :success
  end

  test "should update estado_lectura" do
    patch estado_lectura_url(@estado_lectura), params: { estado_lectura: { nombre: @estado_lectura.nombre } }
    assert_redirected_to estado_lectura_url(@estado_lectura)
  end

  test "should destroy estado_lectura" do
    assert_difference('EstadoLectura.count', -1) do
      delete estado_lectura_url(@estado_lectura)
    end

    assert_redirected_to estado_lecturas_url
  end
end
