class CreateAsignacionOrdens < ActiveRecord::Migration[5.0]
  def change
    create_table :asignacion_ordens do |t|
      t.references :orden_trabajo, foreing_key: true
      t.references :usuario, foreign_key: true

      t.timestamps
    end
  end
end
