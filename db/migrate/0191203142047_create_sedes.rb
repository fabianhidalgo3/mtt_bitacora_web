class CreateSedes < ActiveRecord::Migration[5.0]
  def change
    create_table :sedes do |t|
      t.string :nombre
      t.references :region, foreign_key: true

      t.timestamps
    end
  end
end
