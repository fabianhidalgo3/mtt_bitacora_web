require 'test_helper'

class TipoLecturasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_lectura = tipo_lecturas(:one)
  end

  test "should get index" do
    get tipo_lecturas_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_lectura_url
    assert_response :success
  end

  test "should create tipo_lectura" do
    assert_difference('TipoLectura.count') do
      post tipo_lecturas_url, params: { tipo_lectura: { nombre: @tipo_lectura.nombre } }
    end

    assert_redirected_to tipo_lectura_url(TipoLectura.last)
  end

  test "should show tipo_lectura" do
    get tipo_lectura_url(@tipo_lectura)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_lectura_url(@tipo_lectura)
    assert_response :success
  end

  test "should update tipo_lectura" do
    patch tipo_lectura_url(@tipo_lectura), params: { tipo_lectura: { nombre: @tipo_lectura.nombre } }
    assert_redirected_to tipo_lectura_url(@tipo_lectura)
  end

  test "should destroy tipo_lectura" do
    assert_difference('TipoLectura.count', -1) do
      delete tipo_lectura_url(@tipo_lectura)
    end

    assert_redirected_to tipo_lecturas_url
  end
end
