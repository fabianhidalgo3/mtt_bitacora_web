require 'test_helper'

class EmpresaSubempresasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @empresa_subempresa = empresa_subempresas(:one)
  end

  test "should get index" do
    get empresa_subempresas_url
    assert_response :success
  end

  test "should get new" do
    get new_empresa_subempresa_url
    assert_response :success
  end

  test "should create empresa_subempresa" do
    assert_difference('EmpresaSubempresa.count') do
      post empresa_subempresas_url, params: { empresa_subempresa: { empresa_id: @empresa_subempresa.empresa_id, subempresa_id: @empresa_subempresa.subempresa_id } }
    end

    assert_redirected_to empresa_subempresa_url(EmpresaSubempresa.last)
  end

  test "should show empresa_subempresa" do
    get empresa_subempresa_url(@empresa_subempresa)
    assert_response :success
  end

  test "should get edit" do
    get edit_empresa_subempresa_url(@empresa_subempresa)
    assert_response :success
  end

  test "should update empresa_subempresa" do
    patch empresa_subempresa_url(@empresa_subempresa), params: { empresa_subempresa: { empresa_id: @empresa_subempresa.empresa_id, subempresa_id: @empresa_subempresa.subempresa_id } }
    assert_redirected_to empresa_subempresa_url(@empresa_subempresa)
  end

  test "should destroy empresa_subempresa" do
    assert_difference('EmpresaSubempresa.count', -1) do
      delete empresa_subempresa_url(@empresa_subempresa)
    end

    assert_redirected_to empresa_subempresas_url
  end
end
