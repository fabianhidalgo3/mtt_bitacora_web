class CreateSedeUsuarios < ActiveRecord::Migration[5.0]
  def change
    create_table :sedes_usuarios do |t|
      t.references :sede, foreign_key: true
      t.references :usuario, foreign_key: true

      t.timestamps
    end
  end
end
