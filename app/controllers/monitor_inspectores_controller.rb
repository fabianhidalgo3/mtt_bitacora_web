class MonitorInspectoresController < ApplicationController
  require 'savon'
  require 'json'

  def index
    @regiones = Region.all
    @eventos = Evento.all
    fechaActual = Time.zone.now.strftime("%Y-%m-%e")
    p response_ot_wsdl(fechaActual)
    p response_usuario_wsdl(fechaActual)
  end

  # WSDL getMapDataOt
  # Lista de Tareas de Siftra
  def response_ot_wsdl(fecha)
    client = Savon.client(wsdl: "http://190.196.5.34:7125/ServiciosMapGeo/services/getMapDataSEImplPort?wsdl")
    response = client.call(:get_map_data_ot, message:{arg0: fecha})
    response = response.body[:get_map_data_ot_response][:return]
    ordenesJson =  JSON.parse(response)
    ordenesJson.each do |orden|
       id =  orden["id"]
       fechaInicio = orden["fechaInicio"]
       fechaTermino = orden["fechaTermino"]
       idEstado = orden["idEstado"]
       nombreEstado = orden["nombreEstado"]
       idNombreTarea = orden["idNombreTarea"]
       nombreTarea = orden["nombreTarea"]
       fechaCitacion = orden["fechaCitacion"]
       idTipoServicio = orden["idTipoServicio"]
       tipoServicio = orden["tipoServicio"]
       idPuntoControl = orden["idPuntoControl"]
       nombrePuntoControl = orden["nombrePuntoControl"]
       codigoPuntoContol = orden["codigoPuntoControl"]
       idDireccion = orden["idDireccion"]
       callePuntoControl = orden["callePuntoControl"]
       numeroPuntoControl = orden["numeroPuntoControl"]
       interseccionPuntoControl = orden["interseccionPuntoControl"]
       longitudPuntoControl = orden["longitudPuntoControl"]
       latitudPuntoControl = orden["latitudPuntoControl"]
       idComuna = orden["idComuna"]
       nombreComuna = orden["nombreComuna"]
       idRegion = orden["idRegion"]
       nombreRegion = orden["nombreRegion"]
       idTurno = orden["idTurno"]
       turno = orden["turno"]
       idSentido = orden["idSentido"]
       sentido = orden["sentido"]
       correlativoSolicitud = orden["correlativoSolicitud"]
       idSede = orden["idSede"]
       sede = orden["sede"]
       fechaModificacion = orden["fechaModificacion"]
      ordenTrabajo = OrdenTrabajo.where(id: id)
      if idSentido == 0
        idSentido = 7
      end
      if ordenTrabajo.blank?

      ordenTrabajo = OrdenTrabajo.create(
        id: id,
        fecha_inicio: fechaInicio,
        fechatermino: fechaTermino,
        estado_id: idEstado,
        tarea_id: idNombreTarea,
        tipo_servicio_id: idTipoServicio,
        punto_control_id: idPuntoControl,
        comuna_id: idComuna,
        region_id: idRegion,
        turno_id: idTurno,
        sentido_id: idSentido,
        direccion_id: idDireccion,
        sede_id: idSede,
        correlativo_solicitud: correlativoSolicitud
      )
      else
        p "Orden Lectura Existe"
      end
    end
  end

  # WSDL getMapDataUser
  # Lista de Usuarios de Siftra
  def response_usuario_wsdl(fecha)
    client = Savon.client(wsdl: "http://190.196.5.34:7125/ServiciosMapGeo/services/getMapDataSEImplPort?wsdl")
    usuario = client.call(:get_map_data_user, message:{arg0: fecha})
    usuario = usuario.body[:get_map_data_user_response][:return]
    listaUsuarios =  JSON.parse(usuario)
    p listaUsuarios
    listaUsuarios.each do |usuario|
      id = usuario["id"]
      nombre = usuario["nombre"]
      codigo = usuario["codigo"]
      idSede = usuario["idSede"]
      sede = usuario["sede"]
      idRegion = usuario["idRegion"]
      region = usuario["region"]
      ot = usuario["ot"]
      usuario = Usuario.where(email: nombre)
      if usuario.blank?
        usuario = Usuario.create(email: nombre, codigo: codigo, sede_id: idSede, region_id: idRegion)
      end
    end
  end

  # GET
  # Devuelve Objeto @comunas
  def carga_comunas
    region_id = params[:region].to_i
    @comunas = Comuna.where(region_id: region_id).order("comunas.nombre ASC")
    p @comunas
  end

  # GET
  # Devuelve Objeto @sedes
  def carga_sedes
    region_id = params[:region].to_i
    @sedes = Sede.where(region_id: region_id)
  end

  # GET
  # Devuelve Objeto @usuarios
  def carga_usuarios
    sede_id = params[:sede].to_i
    @usuarios = Usuario.joins(:sede).where(sedes: {id: sede_id}).group(:id).order(:email)
    p @usuarios
  end

end
